import { Injectable} from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const url = 'http://localhost:5000/';

export interface GameState {
  board: string[][];
  player: number;
}

@Injectable()
export class CheckersService {
  constructor(private http: HttpClient) {}

  getNewGame(): Observable<GameState> {
    return this.http.get<GameState>(url + 'new_game');
  }

  getBestMove(gameState): Observable<number[][]> {
    return this.http.post<number[][]>(url + 'best_move', {'state': gameState});
  }

  getNextState(gameState, move): Observable<GameState> {
    return this.http.post<GameState>(url + 'next_state', {'state': gameState, 'move': move});
  }

  toString(gameState): Observable<string> {
    return this.http.post<string>(url, gameState);
  }
}
