import {Component, OnInit} from '@angular/core';
import { CheckersService, GameState } from './checkers.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  gameState: GameState = null;
  move: number[][] = null;

  constructor(private checkers: CheckersService) {}

  ngOnInit() {
    this.getNewGame();
  }

  getNewGame() {
    this.checkers.getNewGame().subscribe(
      data => {
        this.gameState = data;
        console.log('getNewGame(): ' + data);
      },
      err => console.error('getNewGame(): ' + err),
      () => this.getBestMove()
    );
  }

  getBestMove() {
    this.checkers.getBestMove(this.gameState).subscribe(
      data => {
        this.move = data;
        console.log('getBestMove(): ' + data);
      },
      err => console.error('getBestMove(): ' + err),
      //() => //this.waittomove()//this.getNextState()
    );
  }

  waitingtomove() {

    this.getNextState()

  }

  getNextState() {
    this.checkers.getNextState(this.gameState, this.move).subscribe(
      data => {
        this.gameState = data;
        console.log('getNextState(): ' + data);
      },
      err => console.error('getNextState(): ' + err),
      () => this.getBestMove()
    );

    //this.move = null;
  }

  charToLink(row: number, col: number): string {
    if (this.gameState.board[row][col] === '●') {
      return 'black-piece.png';
    } else if (this.gameState.board[row][col] === '◕') {
      return 'black-king.png';
    } else if (this.gameState.board[row][col] === '○') {
      return 'red-piece.png';
    } else if (this.gameState.board[row][col] === '◔') {
      return 'red-king.png';
    } else {
      return null;
    }
  }

  moveOrigin(row: number, col: number): boolean {

    if (this.move == null) {return false;}
    else {

      if ((this.move[0][0] == row) && (this.move[0][1] == col)) {
        return true;
      }
      else {
        return false;
      }
    }

  }

  moveDest(row: number, col: number): boolean {

    if ((this.move[1][0] == row) && (this.move[1][1] == col)) {
      return true;
    }
    else {
      return false;
    }

  }
}
